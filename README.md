# Infra Setup timeline
Friday (3/9): Research work, deciding on the technical components.
Saturday (4/9): Drew the architecture diagram, did some technical feasibility to see if everything works.
Sunday (5/9): Settled the cloudformation script. Got it running according to the architecture diagram.
Monday (6/9): Set up the DB component. Infra components are all up. In terms of implementation, have cut down the RDS instances to be only in 1 AZ due to cost reasons.
Tuesday (7/9): I just realised application is in docker... Due to time constraints, can only proceed with existing design.
Wednesday (7/9): Really have no time to complete the fullstack code challenge due to work responsibilities :( Added the devops workflow diagrams.  I have done up some of the backend codes, but its all raw from template, I've simply edited the serverless.yaml only due to time constraints.. Anyways, I had fun doing the code challenge regardless :)

# Note
This repo does not run, it simply contains the running changes for the infra components.

## Network
This cloudformation script deploys a VPC, with a pair of public and private subnets spread across two Availability Zones. It deploys an internet gateway, with a default route on the public subnets. It deploys a pair of NAT gateways (one in each AZ), and default routes for them in the private subnets. 

When deploying the stack on cloudformation, stack name and environment have to be passed in as parameters. It should deploy all the necessary components as described in the architecture diagram - with exceptions to the Standby DB due to cost constraints.

<!-- ## Frontend 
This cloudformation script deploys an S3 bucket hooked on to a cloudfront distribution.  -->
## Backend
The same cloudfront distribution is hooked up to gateway that will integrate to lambda through serverless.
## Naming conventions
I have followed the below naming conventions:
`{team} - {project} - {environment} - {component}`

These are the values I used:
Organisation/Team - govtech
Project - safeentry
Environment - dev / test / prod
Component - frontend / backend - usually this can be broken down into several parts depending on the project.
